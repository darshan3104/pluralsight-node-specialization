// import { createServer } from "http";
// const server = createServer((req, res) => {
//   res.end("Hello World\n");
//

const express = require("express");
const debug = require("debug")("app");
const morgan = require("morgan");
const app = express();

app.use(morgan("tiny"));

const PORT = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.send("Hello from express");
});

app.listen(PORT, () => {
  debug("Server is running...");
});
