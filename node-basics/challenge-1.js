/**
 *  hello after 4 seconds
 *  hello after 8 seconds
 *
 *  You can define only ONE function
 */

const theOneFunc = () => {
  console.log("Hello after 4 seconds");
  setTimeout(() => {
    console.log("Hello after 8 seconds");
  }, 12 * 1000);
};

setTimeout(theOneFunc, 4 * 1000);
