const express = require("express");
const debug = require("debug")("app");
const morgan = require("morgan");
const app = express();

app.use(morgan("tiny"));
const PORT = process.env.PORT || 3000;

app.set("views", "./src/views");
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  res.render("index", { title: "Welcome to globomantics" });
});

app.listen(PORT, () => {
  debug(`Server is running on ${PORT}...`);
});
